-- create table migrated_scores
-- select * from periods order by id desc;
use sca;
set @period_id = 68;
select st.serial_number, sb.key, fc_GetScore(gs.id, s.subject_id, 0) -- 0 promedio ordinario, 1 promedio extras
	from group_student gs 
    inner join groups g on g.id = gs.group_id 
    and g.period_id = @period_id 
    inner join schedules s on s.group_id = g.id 
    inner join students st on st.id = gs.student_id 
    inner join subjects sb on sb.id = s.subject_id
    -- where sb.key in (0,1,2,3)
	 where g.id in (1906)
	-- and fc_GetScore(gs.id, s.subject_id, 1) > 0
    -- where fc_GetScore(gs.id, s.subject_id, 1) > 0
   and serial_number = '0317114970'
order by gs.id


-- crear una tabla temporal de calificaciones en saiiut y realizar inserts con los registros encontrados
-- en este query.